package jhttp

import (
	"fmt"
)

import(
	"net/http"
	."github.com/wangjiateng/jhttp/dispatch"
)

type Jhttp struct{
	_config *Jconfig
}

func (_this Jhttp) RunOnAddr(addr string) error {
	_dispatch := &JDispatch{}
	http.HandleFunc("/",_dispatch.Dispatch)
	return http.ListenAndServe(addr,nil)
}

func (_this Jhttp) Run() error{
	return _this.RunOnAddr(":4001")
}